/*
 * Copyright 2024 Daniel Espinosa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Top level {@link GXml.Element} describing a project
 */
public class Gpxi.Project : GXml.Element
{
    construct {
        initialize_with_namespace ("Project", "", "http://schemas.microsoft.com/project");
    }
}

/**
 * The version of Project that saved the project XML file. SaveVersion = 12 for Microsoft Office Project 2007.
 */
public class Gpxi.SaveVersion : GXml.Element
{
    construct {
        initialize ("SaveVersion");
    }
}

/**
 * The build number of Microsoft Project that was used to create the xml file.
 */
public class Gpxi.BuildNumber : GXml.Element
{
    construct {
        initialize ("BuildNumber");
        text_content = "16.0.17231.20194"; // Any value
    }
}

/**
 * The globally unique identifier (GUID) of the project.
 */
public class Gpxi.GUID : GXml.Element
{
    construct {
        initialize ("GUID");
        text_content = "";
    }
}

/**
 * Project file name, for example, ProjectName.xml.
 */
public class Gpxi.Name : GXml.Element
{
    construct {
        initialize ("Name");
    }
}

/**
 * Title of the project. Deprecated in Project 2007.
 */
public class Gpxi.Title : GXml.Element
{
    construct {
        initialize ("Title");
    }
}

/**
 * Subject of the project.
 */
public class Gpxi.Subject : GXml.Element
{
    construct {
        initialize ("Subject");
    }
}

/**
 * Category the project belongs to.
 */
public class Gpxi.Category : GXml.Element
{
    construct {
        initialize ("Category");
    }
}

/**
 * Name of the company that created the project.
 */
public class Gpxi.Company : GXml.Element
{
    construct {
        initialize ("Company");
    }
}

/**
 * Manager of the project.
 */
public class Gpxi.Manager : GXml.Element
{
    construct {
        initialize ("Manager");
    }
}

/**
 * Author of the project.
 */
public class Gpxi.Author : GXml.Element
{
    construct {
        initialize ("Author");
    }
}

/**
 * Date the project was created.
 */
public class Gpxi.CreationDate : GXml.Element
{
    construct {
        initialize ("CreationDate");
    }
}

/**
 * Number of times that the project has been saved.
 */
public class Gpxi.Revision : GXml.Element
{
    construct {
        initialize ("Revision");
    }
}

/**
 * Date the project was last saved.
 */
public class Gpxi.LastSaved : GXml.Element
{
    construct {
        initialize ("LastSaved");
    }
}

/**
 * Indicates whether the project is scheduled from its start date or finish date.
 */
public class Gpxi.ScheduleFromStart : GXml.Element
{
    construct {
        initialize ("ScheduleFromStart");
    }
}

/**
 * Date and time that a project is scheduled to begin; required if ScheduleFromStart is true.
 */
public class Gpxi.StartDate : GXml.Element
{
    construct {
        initialize ("StartDate");
    }
}

/**
 * Date and time that a project is scheduled to end; required if ScheduleFromStart is false.
 */
public class Gpxi.FinishDate : GXml.Element
{
    construct {
        initialize ("FinishDate");
    }
}

/**
 * Month the fiscal year begins.
 */
public class Gpxi.FYStartDate : GXml.Element
{
    construct {
        initialize ("FYStartDate");
    }
}

/**
 * Number of days past its end date that a task can go before Project marks that task as a critical task.
 */
public class Gpxi.CriticalSlackLimit : GXml.Element
{
    construct {
        initialize ("CriticalSlackLimit");
    }
}

/**
 * Number of digits that appear after the decimal when Project shows currency values.
 */
public class Gpxi.CurrencyDigits : GXml.Element
{
    construct {
        initialize ("CurrencyDigits");
    }
}

/**
 * Currency symbol used to represent the type of currency used in the project.
 */
public class Gpxi.CurrencySymbol : GXml.Element
{
    construct {
        initialize ("CurrencySymbol");
    }
}

/**
 * New in Project 2007. Three-letter currency character code as defined in ISO 4217; for example, EUR for Euros.
 */
public class Gpxi.CurrencyCode : GXml.Element
{
    construct {
        initialize ("CurrencyCode");
    }
}

/**
 * Indicates the placement of the currency symbol in relation to the currency value:
 */
public class Gpxi.CurrencySymbolPosition : GXml.Element
{
    construct {
        initialize ("CurrencySymbolPosition");
    }
}

/**
 * Unique ID for the calendar used in the project.
 */
public class Gpxi.CalendarUID : GXml.Element
{
    construct {
        initialize ("CalendarUID");
    }
}

/**
 * Default start time for all new tasks.
 */
public class Gpxi.DefaultStartTime : GXml.Element
{
    construct {
        initialize ("DefaultStartTime");
    }
}

/**
 * Default finish time for all new tasks.
 */
public class Gpxi.DefaultFinishTime : GXml.Element
{
    construct {
        initialize ("DefaultFinishTime");
    }
}

/**
 * Default number of minutes per day.
 */
public class Gpxi.MinutesPerDay : GXml.Element
{
    construct {
        initialize ("MinutesPerDay");
    }
}

/**
 * Default number of minutes per week.
 */
public class Gpxi.MinutesPerWeek : GXml.Element
{
    construct {
        initialize ("MinutesPerWeek");
    }
}

/**
 * Default number of working days per month.
 */
public class Gpxi.DaysPerMonth : GXml.Element
{
    construct {
        initialize ("DaysPerMonth");
    }
}

/**
 * Default type for all new tasks in the project.
 */
public class Gpxi.DefaultTaskType : GXml.Element
{
    construct {
        initialize ("DefaultTaskType");
    }
}

/**
 * Default part of the project when fixed costs are accrued (start, prorated, or end).
 */
public class Gpxi.DefaultFixedCostAccrual : GXml.Element
{
    construct {
        initialize ("DefaultFixedCostAccrual");
    }
}

/**
 * Default standard rate for new resources.
 */
public class Gpxi.DefaultStandardRate : GXml.Element
{
    construct {
        initialize ("DefaultStandardRate");
    }
}

/**
 * Default overtime rate for new resources.
 */
public class Gpxi.DefaultOvertimeRate : GXml.Element
{
    construct {
        initialize ("DefaultOvertimeRate");
    }
}

/**
 * Default format for all durations in the project
 */
public class Gpxi.DurationFormat : GXml.Element
{
    construct {
        initialize ("DurationFormat");
    }
}

/**
 * Default format for all work durations in the project.
 */
public class Gpxi.WorkFormat : GXml.Element
{
    construct {
        initialize ("WorkFormat");
    }
}

/**
 * Indicates whether Project automatically calculates actual costs.
 */
public class Gpxi.EditableActualCosts : GXml.Element
{
    construct {
        initialize ("EditableActualCosts");
    }
}

/**
 * Indicates whether Project schedules tasks according to their constraint dates instead of any task dependencies.
 */
public class Gpxi.HonorConstraints : GXml.Element
{
    construct {
        initialize ("HonorConstraints");
    }
}

/**
 * Indicates whether Project schedules tasks according to their constraint dates instead of any task dependencies.
 */
public class Gpxi.EarnedValueMethod : GXml.Element
{
    construct {
        initialize ("EarnedValueMethod");
    }
}

/**
 * Indicates whether inserted projects are treated as summary tasks rather than as separate projects for schedule calculation.
 */
public class Gpxi.InsertedProjectsLikeSummary : GXml.Element
{
    construct {
        initialize ("InsertedProjectsLikeSummary");
    }
}

/**
 * Indicates whether Project calculates and displays a critical path for each independent network of tasks within a project.
 */
public class Gpxi.MultipleCriticalPaths : GXml.Element
{
    construct {
        initialize ("MultipleCriticalPaths");
    }
}

/**
 * Indicates whether new tasks are effort-driven.
 */
public class Gpxi.NewTasksEffortDriven : GXml.Element
{
    construct {
        initialize ("NewTasksEffortDriven");
    }
}

/**
 * Indicates whether new tasks have estimated durations.
 */
public class Gpxi.NewTasksEstimated : GXml.Element
{
    construct {
        initialize ("NewTasksEstimated");
    }
}

/**
 * Indicates whether in-progress tasks may be split.
 */
public class Gpxi.SplitsInProgressTasks : GXml.Element
{
    construct {
        initialize ("SplitsInProgressTasks");
    }
}

/**
 * Indicates whether actual costs are spread to the status date.
 */
public class Gpxi.SpreadActualCost : GXml.Element
{
    construct {
        initialize ("SpreadActualCost");
    }
}

/**
 * Indicates whether percent complete is spread to the status date.
 */
public class Gpxi.SpreadPercentComplete : GXml.Element
{
    construct {
        initialize ("SpreadPercentComplete");
    }
}

/**
 * Indicates whether updates to tasks update resources.
 */
public class Gpxi.TaskUpdatesResource : GXml.Element
{
    construct {
        initialize ("TaskUpdatesResource");
    }
}

/**
 * Indicates whether fiscal year numbering is used.
 */
public class Gpxi.FiscalYearStart : GXml.Element
{
    construct {
        initialize ("FiscalYearStart");
    }
}

/**
 * Start day of the week.
 */
public class Gpxi.WeekStartDay : GXml.Element
{
    construct {
        initialize ("WeekStartDay");
    }
}

/**
 * Indicates whether the end of completed portions of tasks scheduled
 *  to begin after the status date, but begun early, should be moved back
 *  to the status date.
 */
public class Gpxi.MoveCompletedEndsBack : GXml.Element
{
    construct {
        initialize ("MoveCompletedEndsBack");
    }
}

/**
 * Indicates whether remaining portions of tasks scheduled to begin
 * after the status date, but begun early, should be moved back
 * to the status date.
 */
public class Gpxi.MoveRemainingStartsBack : GXml.Element
{
    construct {
        initialize ("MoveRemainingStartsBack");
    }
}

/**
 * Indicates whether remaining portions of tasks scheduled to have
 * begun late should be moved up to the status date.
 */
public class Gpxi.MoveRemainingStartsForward : GXml.Element
{
    construct {
        initialize ("MoveRemainingStartsForward");
    }
}

/**
 * Indicates whether completed portions of tasks scheduled to have
 * been completed before the status date, but begun late, should be
 * moved up to the status date.
 */
public class Gpxi.MoveCompletedEndsForward : GXml.Element
{
    construct {
        initialize ("MoveCompletedEndsForward");
    }
}

/**
 * Specific baseline used to calculate variance values.
 */
public class Gpxi.BaselineForEarnedValue : GXml.Element
{
    construct {
        initialize ("BaselineForEarnedValue");
    }
}

/**
 * Indicates whether to automatically add new resources to the resource pool.
 */
public class Gpxi.AutoAddNewResourcesAndTasks : GXml.Element
{
    construct {
        initialize ("AutoAddNewResourcesAndTasks");
    }
}

/**
 * Date used for calculation and reporting.
 */
public class Gpxi.StatusDate : GXml.Element
{
    construct {
        initialize ("StatusDate");
    }
}

/**
 * The system date that Project generated the XML file.
 */
public class Gpxi.CurrentDate : GXml.Element
{
    construct {
        initialize ("CurrentDate");
    }
}

/**
 * Indicates whether the project was created by a user who logged on with
 * Windows authentication or Forms authentication.
 */
public class Gpxi.MicrosoftProjectServerURL : GXml.Element
{
    construct {
        initialize ("MicrosoftProjectServerURL");
    }
}

/**
 * Indicates whether to automatically link inserted or moved tasks.
 */
public class Gpxi.Autolink : GXml.Element
{
    construct {
        initialize ("Autolink");
    }
}

/**
 * Default start date for a new task.
 */
public class Gpxi.NewTaskStartDate : GXml.Element
{
    construct {
        initialize ("NewTaskStartDate");
    }
}

/**
 * Default earned value method for tasks.
 */
public class Gpxi.DefaultTaskEVMethod : GXml.Element
{
    construct {
        initialize ("DefaultTaskEVMethod");
    }
}

/**
 * Indicates whether the project was edited externally.
 */
public class Gpxi.ProjectExternallyEdited : GXml.Element
{
    construct {
        initialize ("ProjectExternallyEdited");
    }
}

/**
 * Date used for calculation and reporting.
 */
public class Gpxi.ExtendedCreationDate : GXml.Element
{
    construct {
        initialize ("ExtendedCreationDate");
    }
}

/**
 * Indicates whether all actual work is synchronized with the project.
 */
public class Gpxi.ActualsInSync : GXml.Element
{
    construct {
        initialize ("ActualsInSync");
    }
}

/**
 * Indicates whether to remove certain file properties on save, such as Author, Manager, and Company.
 */
public class Gpxi.RemoveFileProperties : GXml.Element
{
    construct {
        initialize ("RemoveFileProperties");
    }
}

/**
 * Indicates whether the project is an administrative project.
 */
public class Gpxi.AdminProject : GXml.Element
{
    construct {
        initialize ("AdminProject");
    }
}

/**
 * Collection of local outline code definitions in a project.
 */
public class Gpxi.OutlineCodes : GXml.Element
{
    construct {
        initialize ("OutlineCodes");
    }
}

/**
 * Collection of extended attribute (custom field) definitions in a project.
 */
public class Gpxi.ExtendedAttributes : GXml.Element
{
    construct {
        initialize ("ExtendedAttributes");
    }
}

/**
 * Collection of calendars associated with a project.
 */
public class Gpxi.Calendars : GXml.Element
{
    construct {
        initialize ("Calendars");
    }
}

/**
 * Collection of tasks that make up a project.
 */
public class Gpxi.Tasks : GXml.Element
{
    construct {
        initialize ("Tasks");
    }
}

/**
 * Collection of resources that make up a project.
 */
public class Gpxi.Resources : GXml.Element
{
    construct {
        initialize ("Resources");
    }
}

/**
 * Collection of assignments in a project.
 */
public class Gpxi.Assignments : GXml.Element
{
    construct {
        initialize ("Assignments");
    }
}

/**
 * Collection of Enterprise Custom Fields with lookup table values
 */
public class Gpxi.EnterpriseExtendedAttribute : GXml.Element
{
    construct {
        initialize ("EnterpriseExtendedAttribute");
    }
}


